define([], function() {	
	sap.m.Slider.extend("com.iprosis.sapui5.Slider", {
		initDesignStudio: function() {
			this.attachChange(function() {
				this.fireDesignStudioPropertiesChanged(["value"]);
				this.fireDesignStudioEvent("onChange");
			});
		},
		renderer: {}
	});
	
	sap.m.RangeSlider.extend("com.iprosis.sapui5.RangeSlider", {
		initDesignStudio: function() {
			this.attachChange(function() {
				this.fireDesignStudioPropertiesChanged(["value"]);
				this.fireDesignStudioPropertiesChanged(["value2"]);
				this.fireDesignStudioEvent("onChange");
			});
		},
		renderer: {}
	});
});